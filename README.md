# alphasocket/dockerized-toolbox-alpine
#### toolbox-alpine
This docker image will contain all required cli tools for security/development/testing operations. 
toolboxctl is the interface used to manage the container and the modules 
 
- Core 
  + Core var and functions 
 
- Main 
  + Handle the main container where the user will log in 
 
- NFS 
  + It's included an nfs server to mount persistent volumes from the home directory see [NFS documentation](http://wiki.linux-nfs.org/wiki/index.php/Nfsv4_configuration) 
  + Based on [ehough/docker-nfs-server](https://github.com/ehough/docker-nfs-server/tree/master)

## Branches & Versions
- latest


## Packages installed


## Configurable envvars
~~~
CONFIG_VERBOSE="True"
CONFIG_GROUPS_MAIN_ID="1000"
CONFIG_GROUPS_MAIN_NAME="docker"
CONFIG_GROUPS_ADDITIONAL_ID="1001"
CONFIG_GROUPS_ADDITIONAL_NAME=""
CONFIG_USERS_MAIN_ID="1000"
CONFIG_USERS_MAIN_NAME="docker"
CONFIG_USERS_MAIN_GROUPS="docker"
CONFIG_USERS_ADDITIONAL_ID="1001"
CONFIG_USERS_ADDITIONAL_NAME=""
CONFIG_USERS_ADDITIONAL_GROUPS=""
CONFIG_READINESS_TEST="true"
CONFIG_LIVENESS_TEST="true"
CONFIG_PATHS_CONTAINER_STATUS="/tmp/container_status"
~~~
