#!/bin/bash
#
# Manage NFS exporting tools for Toolbox (CLI container)
#
# SG: https://google.github.io/styleguide/shell.xml
# https://docs.google.com/document/d/1gkRfW2xCWA9UhpciJWWeRtMX0F125mz9-ceF6kRrhJo/edit#
#



DOCKER_COMPOSE="$(which docker-compose)"

PROJECTS_PATH="${PROJECTS_PATH-$HOME/Projects}"
BINARIES_PATH="${BINARIES_PATH-$HOME/bin}"
LIBRARIES_PATH="${LIBRARIES_PATH-$HOME/lib}"

CONTAINER_MANAGER="${CONTAINER_MANAGER-docker-compose}"

TOOLBOX_BINARIES_PATH="${LIBRARIES_PATH}/toolbox"
TOOLBOXCTL_SOURCE_PATH="$(readlink -f $0)"
TOOLBOXCTL_TARGET_PATH="${BINARIES_PATH}/toolboxctl"

DOCKER_SOCKET_PATH="${DOCKER_SOCKET_PATH:-/var/run/docker.sock}"

DOCKER_VOLUMES=" -v $DOCKER_SOCKET_PATH:$DOCKER_CONTAINER_PATH "

#
# NFS SERVICE
#
NFS_CONTAINER_PRIVILIGED="${NFS_CONTAINER_PRIVILIGED:-true}"
NFS_CONTAINER_NAME="toolbox_nfs_server"
NFS_CONTAINER_MODE="-d"
NFS_CONTAINER_IP="${NFS_CONTAINER_IP-127.0.0.1}"
NFS_CONTAINER_PORT="${NFS_CONTAINER_PORT:-2049}"
if [ -z $NFS_CONTAINER_IMAGE ]; then
    NFS_CONTAINER_IMAGE="alphasocket/toolbox-alpine:latest"
fi
NFS_CONTAINER_COMMAND="nfsd"
NFS_CONTAINER_BINARIES_PATH="${NFS_CONTAINER_BINARIES_PATH:-/usr/local/bin}"
NFS_CONTAINER_LIBRARIES_PATH="${NFS_CONTAINER_LIBRARIES_PATH:-/usr/local/lib}"
NFS_IP_PERMITTED="${NFS_IP_PERMITTED:-127.0.0.1}"
#NFS_IP_PERMITTED="${NFS_IP_PERMITTED:127.0.0.1,192.168.*.*,10.*.*.*,172.16-31.*.*}"
NFS_EXPORT_0="/ 127.0.0.1(fsid=0,rw,sync,no_subtree_check,no_auth_nlm,insecure,no_root_squash,verify_dir)"
NFS_DOCKER_COMPOSE=$( cat <<YAML
version: "3.5"
services:
    toolbox_nfs_server:
        container_name: ${NFS_CONTAINER_NAME}
        image: ${NFS_CONTAINER_IMAGE}
        privileged: ${NFS_CONTAINER_PRIVILIGED}
        entrypoint: ["tini","--","docker-entrypoint"]
        command: ["${NFS_CONTAINER_COMMAND}"]
        restart: always
        working_dir: $HOME
        network_mode: host
        environment:
            - HOME=$HOME
            - UID=$UID
            - GID=$GID
            - WHOAMI=$WHOAMI
            - NFS_DISABLE_VERSION_3=true
            - NFS_EXPORT_0=$NFS_EXPORT_0
            - CONFIG_NFS_SHARED_DIRECTORY=/var/nfsshare
            - CONFIG_NFS_IP_PERMITTED=$NFS_IP_PERMITTED
            - CONFIG_NFS_SYNC=async
        volumes:
            - $DOCKER_CONTAINER_PATH:/var/run/docker.sock:ro
            - /run/mysqld:/run/mysqld:ro
            - /etc/passwd:/etc/passwd:ro
            - /etc/group:/etc/group:ro
            - $HOME:$HOME
YAML
)

function toolboxctl::nfs::usage(){
    cat <<USAGE
    Usage: 
        toolboxctl -hv module command args

    Example: 
        toolboxctl nfs update 
        > Update module components

        toolboxctl nfs restart
        > Restart containers for nfsd service
        
        toolboxctl nfs exec bash 
        > Drops in shell in container
USAGE

}

function toolboxctl::nfs::initialize(){

    if [ -z "$DOCKER_COMPOSE" ]; then
        err "Docker compose binary not found";
        exit 1;
    fi
    
    if ! lsmod | grep -Eq "^nfs\\s+" && [ ! -d "/sys/module/nfs" ]; then
        err "nfs module is not loaded on the Docker host's kernel (try: modprobe nfs)";
        exit 1;
    fi
    
    if ! lsmod | grep -Eq "^nfsd\\s+" && [ ! -d "/sys/module/nfsd" ]; then
        err "nfsd module is not loaded on the Docker host's kernel (try: modprobe nfsd)";
        exit 1;
    fi
}

function toolboxctl::nfs::destroy(){
    a=0;
}

#
# NFS SERVICE FUNCTIONS
#
function toolboxctl::nfs::get::id(){
    echo $(docker ps -aqf "name=$NFS_CONTAINER_NAME" 2>/dev/null)
}

function toolboxctl::nfs::is::running(){
    CONTAINER_ID="$(toolboxctl::nfs::get::id)";
    [ ! -z "$CONTAINER_ID" ] && [ "$( docker inspect -f {{.State.Running}} $CONTAINER_ID 2> /dev/null)" = 'true' ]
}

function toolboxctl::nfs::get::status(){
    if toolboxctl::nfs::is::running; then
        echo "Running"
    else
        echo "NotRunning"
    fi
}

function toolboxctl::nfs::status(){
    if toolboxctl::nfs::is::running; then
        log "Toolbox not running"
    else
        docker -H unix://$DOCKER_CONTAINER_PATH ps -f "name=$NFS_CONTAINER_NAME"
    fi
}

function toolboxctl::nfs::clean(){
    log "Removing old nfs container"
    if toolboxctl::nfs::is::running; then
        docker -H unix://$DOCKER_CONTAINER_PATH stop "$NFS_CONTAINER_NAME" &> /dev/null || true
    fi
    docker -H unix://$DOCKER_CONTAINER_PATH rm "$NFS_CONTAINER_NAME" --force &> /dev/null || true 
}

function toolboxctl::nfs::start(){
    case $CONTAINER_MANAGER in
        docker-compose)
            echo "$NFS_DOCKER_COMPOSE" | docker-compose -H unix://$DOCKER_CONTAINER_PATH -f - up --detach
            ;;
        *)
            toolboxctl::nfs::clean
            #NFS_EXPORT_0="$HOME/var/data/ *(fsid=0,rw,sync,no_subtree_check,no_auth_nlm,insecure,no_root_squash)"
            #NFS_EXPORT_1="/usr/local/lib *(fsid=1,ro,sync,no_subtree_check,no_auth_nlm,insecure,no_root_squash)"
            
            log "Starting nfs container"
            #docker run -d --name ${NFS_CONTAINER_NAME} --privileged -p 2049:2049 -v $HOME:/nfsshare -e PERMITTED=$PRIVATE_NETWORKS -e SHARED_DIRECTORY=/nfsshare itsthenetwork/nfs-server-alpine:latest
            #docker run -d --name ${NFS_CONTAINER_NAME} --cap-add=NET_ADMIN -p 2049:2049 -v $HOME:/nfsshare -e PERMITTED=$PRIVATE_NETWORKS -e SHARED_DIRECTORY=/nfsshare itsthenetwork/nfs-server-alpine:latest
            docker -H unix://$DOCKER_CONTAINER_PATH run -d \
                --name "$NFS_CONTAINER_NAME" \
                --net host \
                --restart=always \
                $( [ "${NFS_CONTAINER_PRIVILIGED}" = 'true' ] && echo "--privileged" || true) \
                -v $HOME:$HOME \
                -e NFS_DISABLE_VERSION_3='true' \
                -e NFS_EXPORT_0="$NFS_EXPORT_0" \
                -e CONFIG_NFS_IP_PERMITTED="$NFS_IP_PERMITTED" \
                -e HOME=$HOME \
                -w $PWD \
                $NFS_CONTAINER_IMAGE \
                $NFS_CONTAINER_COMMAND
            ;;
    esac
}

function toolboxctl::nfs::stop(){
    case $CONTAINER_MANAGER in
        docker-compose)
            echo "$NFS_DOCKER_COMPOSE" | docker-compose -H unix://$DOCKER_CONTAINER_PATH -f - stop
            ;;
        *)
            log "Stopping nfs container..."
            docker -H unix://$DOCKER_CONTAINER_PATH stop -t 0 "$NFS_CONTAINER_NAME" 1> /dev/null
            ;;
    esac
}


function toolboxctl::nfs::up(){
    if toolboxctl::nfs::is::running; then
        log "Toolbox NFS is up"
    else
        toolboxctl::nfs::start
        toolboxctl::nfs::mount::volumes
    fi
}

function toolboxctl::nfs::down(){
    toolboxctl::nfs::unmount::volumes
    toolboxctl::nfs::stop
    toolboxctl::nfs::clean
}

function toolboxctl::nfs::restart(){
    toolboxctl::nfs::down
    toolboxctl::nfs::up
}


function toolboxctl::nfs::link::binaries(){
    log "Linking binaries..."
    if ! toolboxctl::main::is::running; then
        err "Toolbox needs to be running to link binaries"
        exit 1;
    fi
    
    if [ ! -e "${TOOLBOX_BINARIES_PATH}/runner" ]; then
        err "Runner not found in ${TOOLBOX_BINARIES_PATH}/runner . Can't link binaries "
        exit 1;
    fi
    
    # Recreate toolboxctl link
    unlink "${TOOLBOXCTL_TARGET_PATH}" || true
    ln -s "${TOOLBOXCTL_SOURCE_PATH}" "${TOOLBOXCTL_TARGET_PATH}"
    
    #TOOLBOX_BINARIES="$(toolboxctl exec ls --color=never -l /usr/local/bin | grep ^- | awk '{print $9}' | grep -v docker | grep -v modprobe)"
    #TOOLBOX_BINARIES="$(toolboxctl exec find /usr/local/bin -mindepth 1 -maxdepth 1 -type f -exec basename {} \; | grep -v docker | grep -v modprobe )"
    TOOLBOX_BINARIES="$(toolboxctl exec find ${MAIN_CONTAINER_BINARIES_PATH} -mindepth 1 -maxdepth 1 -type f -exec echo {} \;)"
    for TOOLBOX_BINARY in $TOOLBOX_BINARIES;
    do
        TOOLBOX_BINARY="$(basename ${TOOLBOX_BINARY} )"
        TOOLBOX_BINARY=${TOOLBOX_BINARY//[^[:print:]]/}
        
        case "$TOOLBOX_BINARY" in
            nfsd|docker*|modprobe|setup|config)
                [ -z $VERBOSE ] || log "skipped $TOOLBOX_BINARY"
                continue 2
            ;;
            *)
                TOOLBOX_BINARY_SRC="${TOOLBOX_BINARIES_PATH}/runner"
            ;;
        esac
        
        if [ -e "${TOOLBOX_BINARY_SRC}" ]; then
            log "Linking ${BINARIES_PATH}/${TOOLBOX_BINARY} to ${TOOLBOX_BINARY_SRC} "
            unlink "${BINARIES_PATH}/${TOOLBOX_BINARY}" 2> /dev/null || true;
            ln -s "${TOOLBOX_BINARY_SRC}" "${BINARIES_PATH}/${TOOLBOX_BINARY}";
        fi
    done;
}

#function toolboxctl::nfs::link::libraries(){
#    log "Linking nfs libraries..."
#    if [ ! function toolboxctl::nfs::is::running ]; then
#        err "NFS server needs to be running to mount libraries"
#    fi
#    
#    TOOLBOX_LINKED_BINARIES='/usr/local/bin/minikube /usr/local/bin/hgflow.py'
#    
#    for TOOLBOX_LINKED_BINARY in $TOOLBOX_LINKED_BINARIES;
#    do
#        TOOLBOX_LINKED_BINARY="$(basename ${TOOLBOX_LINKED_BINARY} )"
#        
#        case "$TOOLBOX_LINKED_BINARY" in
#            nfsd*|docker*|modprobe*)
#                [ -z $VERBOSE ] || log "skipped $TOOLBOX_LINKED_BINARY"
#                continue 2
#            ;;
#            *)
#                TOOLBOX_TARGET_LINK="$(toolboxctl exec readlink -nf ${MAIN_CONTAINER_BINARIES_PATH}/${TOOLBOX_LINKED_BINARY} || echo "" )"
#                # Convert link from container_link to host_link
#                TOOLBOX_BINARY_SRC="$( echo $TOOLBOX_TARGET_LINK | sed -e "s?${MAIN_CONTAINER_LIBRARIES_PATH}?${LIBRARIES_PATH}?g" )"
#            ;;
#        esac
#        
#        if [ ! -z "$TOOLBOX_BINARY_SRC" ] && [ -e "${TOOLBOX_BINARY_SRC}" ]; then
#            [ -z $VERBOSE ] || log "Unlinking $TOOLBOX_LINKED_BINARY" 
#            unlink "${BINARIES_PATH}/${TOOLBOX_LINKED_BINARY}" &> /dev/null || true;
#            [ -z $VERBOSE ] || log "Linking ${BINARIES_PATH}/${TOOLBOX_LINKED_BINARY} to ${TOOLBOX_BINARY_SRC} "
#            ln -s "${TOOLBOX_BINARY_SRC}" "${BINARIES_PATH}/${TOOLBOX_LINKED_BINARY}";
#        fi
#    done;
#    
#}

function toolboxctl::nfs::mount::volumes(){
    log "Mounting nfs libraries..."
    
    global::test "docker exec ${NFS_CONTAINER_NAME} docker-rediness-test" \
        "err 'NFS: Container failed, printing log and exiting' && docker logs $NFS_CONTAINER_NAME" \
        true \
        300 1 \
        '[ "$(toolboxctl::nfs::is::running)" = "false" ]'
    
    if docker exec $NFS_CONTAINER_NAME docker-rediness-test; then
        
        for LIB in $(toolboxctl::nfs::get::libraries) ;
        do
            umount ${LIBRARIES_PATH}/$LIB || true
            mkdir -p ${LIBRARIES_PATH}/$LIB &> /dev/null || true
            if ! mount ${LIBRARIES_PATH}/$LIB ; then
                mount_command="sudo mount -v -t nfs4 127.0.0.1:${NFS_CONTAINER_LIBRARIES_PATH}/$LIB ${LIBRARIES_PATH}/$LIB"
                log "Forced mount ${LIBRARIES_PATH}/$LIB : $mount_command"
                toolboxctl::nfs::echo::mounts
                if exec $mount_command; then
                    log "Library $LIB mounted on ${LIBRARIES_PATH}/$LIB ";  
                else
                    log "Library $LIB not mounted ";
                fi
            else
                log "Library $LIB mounted on ${LIBRARIES_PATH}/$LIB ";
            fi
        done;
    fi
}

function toolboxctl::nfs::unmount::volumes(){
    log "Unmounting nfs mounts..."
    for LIB in $(toolboxctl::nfs::get::libraries);
    do
        if [ -e "${LIBRARIES_PATH}/$LIB" ]; then
            ## TODO: add timer in case of force umount needed
            umount "${LIBRARIES_PATH}/$LIB" || true
        fi
    done;
}

function toolboxctl::nfs::get::libraries(){
    LIBS="$(docker exec ${NFS_CONTAINER_NAME} bash -c "find ${NFS_CONTAINER_LIBRARIES_PATH} -type d -maxdepth 1 -mindepth 1 " 2>/dev/null | sed -e "s_${NFS_CONTAINER_LIBRARIES_PATH}/__g" )"
    if [ -z "${LIBS}" ]; then
        LIBS="$(find ${LIBRARIES_PATH} -type d -maxdepth 1 -mindepth 1 2> /dev/null | sed -e "s_${LIBRARIES_PATH}/__g" )"
    fi
    echo $LIBS | sed -e "s/\n\r\t/ /g"
}

function toolboxctl::nfs::echo::mounts(){
    FSTAB_MESSAGE="Add this mounts in /etc/fstab to mount toolbox data without root:\n"
    FSTAB_MESSAGE+="## toolbox-mounts start ##\n"

    for LIB in $(toolboxctl::nfs::get::libraries) ;
    do 
        FSTAB_MESSAGE+="127.0.0.1:${NFS_CONTAINER_LIBRARIES_PATH}/${LIB} ${LIBRARIES_PATH}/${LIB} nfs ro,nfsvers=4,proto=tcp,user,exec,noauto 0 0\n";
    done

    FSTAB_MESSAGE+="## toolbox-mounts end ##\n"

    log "$FSTAB_MESSAGE"
}
