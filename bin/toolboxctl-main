#!/bin/bash
#
# Main Service for Toolbox (CLI container)
#
# SG: https://google.github.io/styleguide/shell.xml
# https://docs.google.com/document/d/1gkRfW2xCWA9UhpciJWWeRtMX0F125mz9-ceF6kRrhJo/edit#
#


TOOLBOX_MAIN_DOCKER_API_URI="unix:///var/run/docker.sock"
TOOLBOX_MAIN_CONTAINER_PRIVILIGED="${TOOLBOX_MAIN_CONTAINER_PRIVILIGED:-false}"
TOOLBOX_MAIN_CONTAINER_USER_CONF=" --privileged ";
TOOLBOX_MAIN_CONTAINER_DOCKER_SOCKET_PATH="${DOCKER_CONTAINER_PATH:-/var/run/docker.sock}"
TOOLBOX_MAIN_SHARED_TMP_FOLDER="${TOOLBOX_MAIN_SHARED_TMP_FOLDER:-/tmp/toolbox/}"
TOOLBOX_MAIN_ENTRYPOINT_VERBOSE="${TOOLBOX_MAIN_ENTRYPOINT_VERBOSE:-False}"
TOOLBOX_MAIN_OVERRIDDES_FOLDER="$HOME/.toolbox/overrides"
TOOLBOX_MAIN_OVERRIDDES=""

if [ -z $TOOLBOX_MAIN_CONTAINER_IMAGE ]; then
    TOOLBOX_MAIN_CONTAINER_IMAGE="registry.gitlab.com/alphasocket_docker/toolbox-alpine:latest"
fi

if [ "$TOOLBOX_MAIN_CONTAINER_PRIVILIGED" = "true" ]; then
    TOOLBOX_MAIN_CONTAINER_USER_CONF=" --privileged ";
else
    if [ -z "$UID" ]; then
        UID=$(id -u);
    fi

    # TODO: make it work with current user group (user is in docker group but can't connect to docker in the container)
    if true || [ -z "$GID" ]; then
        GID=$(getent group docker | cut -d: -f3);
    fi

    TOOLBOX_MAIN_CONTAINER_USER="$UID:$GID"
    TOOLBOX_MAIN_CONTAINER_USER_CONF=" -u ${TOOLBOX_MAIN_CONTAINER_USER} -e UID=$UID -e GID=$GID -e WHOAMI=$WHOAMI "
fi


function toolboxctl::main::usage(){
    cat <<USAGE
    Usage: 
        toolboxctl -hv module command args

    Example: 
        toolboxctl main update 
        > Update module components
        
        toolboxctl main exec bash 
        > Drops in shell in container
USAGE

}

function toolboxctl::main::initialize(){
    mkdir -p $TOOLBOX_MAIN_OVERRIDDES_FOLDER
    mkdir -p $TOOLBOX_MAIN_SHARED_TMP_FOLDER
    
    for override_source in $(find $TOOLBOX_MAIN_OVERRIDDES_FOLDER -type f);
    do
        override_target="${override_source#$TOOLBOX_MAIN_OVERRIDDES_FOLDER}";
        TOOLBOX_MAIN_OVERRIDDES="$TOOLBOX_MAIN_OVERRIDDES -v $override_source:$override_target:ro ";
    done;

    if [ -z "$DOCKER_BIN_PATH" ]; then
        err "Docker client not found";
        exit 1;
    fi
}

function toolboxctl::main::destroy(){
    a=0;
}

function toolboxctl::main::update(){
	docker -H $TOOLBOX_MAIN_DOCKER_API_URI pull $TOOLBOX_MAIN_CONTAINER_IMAGE
}

function toolboxctl::main::exec(){

    if [ $SSH_AUTH_SOCK ]; then
        SSH_AUTH_SOCKET_FOLDER=$(dirname $SSH_AUTH_SOCK)
        SSH_AGENT_OPTIONS="-v $SSH_AUTH_SOCKET_FOLDER:$SSH_AUTH_SOCKET_FOLDER -e SSH_AUTH_SOCK=$SSH_AUTH_SOCK"
        [ $VERBOSE ] && echo $SSH_AGENT_OPTIONS
    fi

    docker -H $TOOLBOX_MAIN_DOCKER_API_URI run -it --rm \
        --cap-add SYS_ADMIN \
        --net host \
        ${TOOLBOX_MAIN_CONTAINER_USER_CONF} \
        -e HOME=$HOME \
        -e DOCKER_HOST=$TOOLBOX_MAIN_DOCKER_API_URI \
        -e ENTRYPOINT_VERBOSE=$TOOLBOX_MAIN_ENTRYPOINT_VERBOSE \
        -v $HOME:$HOME \
        $SSH_AGENT_OPTIONS \
        -v /etc/hosts:/etc/hosts:ro \
        -v /etc/inputrc:/etc/inputrc:ro \
        -v /etc/passwd:/etc/passwd:ro \
        -v /etc/group:/etc/group:ro \
        -v /var/run/docker.sock:/var/run/docker.sock:ro \
        -v /run/mysqld:/run/mysqld:ro \
        -v $TOOLBOX_MAIN_SHARED_TMP_FOLDER:/tmp/ \
        $TOOLBOX_MAIN_OVERRIDDES \
        -w $PWD \
        $TOOLBOX_MAIN_CONTAINER_IMAGE $@
}
