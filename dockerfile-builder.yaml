version: "3"
project: 
  title: &project_title alphasocket/dockerized-toolbox-alpine
  codename: &project_codename toolbox-alpine
  description: "This docker image will contain all required cli tools for security/development/testing operations.
  \ntoolboxctl is the interface used to manage the container and the modules
  \n
  \n- Core
  \n  + Core var and functions
  \n
  \n- Main
  \n  + Handle the main container where the user will log in
  \n 
  \n- NFS
  \n  + It's included an nfs server to mount persistent volumes from the home directory see [NFS documentation](http://wiki.linux-nfs.org/wiki/index.php/Nfsv4_configuration)
  \n  + Based on [ehough/docker-nfs-server](https://github.com/ehough/docker-nfs-server/tree/master)"

#
# Build process
# Creates dockerfile and file used in it
#
build:
  envvars:
    from: docker:dind
    name: *project_codename
    ports:
      main: 2049
    cmd:
      valueFromParse: ${BUILD_PATHS_BINARIES_FOLDER}/pause
    paths:
      bash_completitions: "$HOME/.bash_completion.d"
      binaries:
        folder: /usr/local/bin
      libraries:
        folder: /usr/local/lib
  imports:
    - imports/pause:/usr/local/bin/pause
    - imports/runner:/usr/local/lib/toolbox/runner
    - imports/.welcome:/var/tmp/.welcome

#
# Setup process injected in dockerfile
#
setup:
  # Setup env 
  envvars:
    dependencies:
      setup: gcc py-setuptools openssl-dev jansson-dev python-dev build-base libc-dev file-dev automake autoconf libtool flex linux-headers ghc musl-dev zlib-dev curl bind-tools pv
      utilities: bash bash-doc bash-completion coreutils man man-pages mdocml-apropos vim grep less less-doc curl zip unzip jq htop git mercurial gnupg fcgi openssh-client file tmux tree xz gzip libxml2 libxml2-utils keepassxc rsync
      nfsd: nfs-utils iproute2 
      op: libc6-compat
      php: php7 php7-curl php7-dom php7-gd php7-ctype php7-gettext php7-iconv php7-json php7-mbstring php7-mcrypt php7-mysqli php7-opcache php7-openssl php7-pdo php7-pdo_dblib php7-pdo_mysql php7-pdo_pgsql php7-pdo_sqlite php7-pear php7-pgsql php7-phar php7-posix php7-session php7-soap php7-sockets php7-sqlite3 php7-xml php7-simplexml php7-zip php7-zlib php7-tokenizer php7-xmlwriter
      node: nodejs nodejs-npm
      python: py2-pip python3
      db: mysql-client redis
      google: py-crcmod libc6-compat openssh-client
      go: go
      runtime:
        valueFromParse: $SETUP_DEPENDENCIES_UTILITIES $SETUP_DEPENDENCIES_OP $SETUP_DEPENDENCIES_PHP $SETUP_DEPENDENCIES_NODE $SETUP_DEPENDENCIES_PYTHON $SETUP_DEPENDENCIES_DB $SETUP_DEPENDENCIES_GOOGLE $SETUP_DEPENDENCIES_NFSD $SETUP_DEPENDENCIES_GO
    nfs:
      shared:
        directory: '/var/nfsshare'
      ip:
        permitted: "127.0.0.1/32,192.168.0.1/24,172.16.0.1/16,10.0.0.1/8"
      sync: 'async'
  processes_before:
    - title: "Requirements for download bin and libs"
      commands:
        - mkdir -p "${BUILD_PATHS_BINARIES_FOLDER}"
        - mkdir -p "${BUILD_PATHS_LIBRARIES_FOLDER}"
        - BUILD_PATHS_BASH_COMPLETITIONS="$HOME/.bash_completion.d"
        # get-latest-release
        - curl -fsSL https://raw.githubusercontent.com/AlphaSocket/git-get-latest-release/master/get-latest-release -o ${BUILD_PATHS_BINARIES_FOLDER}/get-latest-release
        - chmod +x ${BUILD_PATHS_BINARIES_FOLDER}/get-latest-release
        - mkdir -p ${BUILD_PATHS_BASH_COMPLETITIONS}/

  processes:
    #- title: "Install NFSD binaries and configure it"
    #  commands:
    #    # NFSD - SemaphoreCI can't handle the NFS 
    #    - curl -fsSL https://raw.githubusercontent.com/ehough/docker-nfs-server/v1.2.0/entrypoint.sh -o ${BUILD_PATHS_BINARIES_FOLDER}/nfsd
    #    - sed -i 's_#!/usr/bin/env bash_#!/bin/bash_g' ${BUILD_PATHS_BINARIES_FOLDER}/nfsd
    #    - echo "rpc_pipefs  /var/lib/nfs/rpc_pipefs  rpc_pipefs  defaults  0  0" >> /etc/fstab 
    #    - echo "nfsd        /proc/fs/nfsd            nfsd        defaults  0  0" >> /etc/fstab
    #    - rm -rf /etc/exports
    #    - mkdir -p /var/lib/nfs/v4recovery
    #    - mkdir -p /var/lib/nfs/rpc_pipefs
    
    # TODO
    - title: "Install extra bash utils"
      commands:
        - axel
        - aria
        - dropbox
        - pip install yamllint
    
    - title: "Install extra php extensions"
      commands:
        - VERSION=$(curl -sSLf https://pecl.php.net/package/xdebug 2> /dev/null | grep -Eio -m1 "redis-([0-9]\.[0-9]\.[0-9])\.tgz" | grep -Eoi -m1 "([0-9]\.[0-9]\.[0-9])")
        - pecl install xdebug-$VERSION
        - VERSION=$(curl -sSLf https://pecl.php.net/package/redis 2> /dev/null | grep -Eio -m1 "redis-([0-9]\.[0-9]\.[0-9])\.tgz" | grep -Eoi -m1 "([0-9]\.[0-9]\.[0-9])")
        - pecl install redis-$VERSION
        - rm -rf /tmp/pear

    - title: "Install extra vcs libraries"
      commands:
        # vcs
        #- curl -fsSL https://bitbucket.org/yujiewu/hgflow/downloads/hgflow-v0.9.8.3.tar.bz2 -o /tmp/hgflow-installer.tar.bz2
        #- mkdir /tmp/hgflow ${BUILD_PATHS_LIBRARIES_FOLDER}/hg
        #- tar -xvjf /tmp/hgflow-installer.tar.bz2 -C /tmp/hgflow/
        #- mv /tmp/hgflow/hgflow.py ${BUILD_PATHS_LIBRARIES_FOLDER}/hg/flow.py
        #- ln -s /usr/local/lib/hg/flow.py /usr/local/bin/hgflow.py
        #- rm -rf /tmp/hgflow
        - curl -fsSl https://www.mercurial-scm.org/repo/hg/raw-file/tip/contrib/bash_completion -o ${BUILD_PATHS_BASH_COMPLETITIONS}/hg
        - pip install hg+https://bitbucket.org/yujiewu/hgflow@4fabf47c309ee9941fd3912f0f4a62133a5c3f51
        - curl -fsSl https://raw.githubusercontent.com/d3m3vilurr/settings/master/.bash_completion.d/hg-flow-completion.bash -o ${BUILD_PATHS_BASH_COMPLETITIONS}/hgflow
        
        #- curl -fsSL https://raw.githubusercontent.com/petervanderdoes/gitflow-avh/develop/contrib/gitflow-installer.sh -o /tmp/gitflow-installer.sh
        #- chmod +x /tmp/gitflow-installer.sh
        #- bash /tmp/gitflow-installer.sh install stable
        #- rm /tmp/gitflow-installer.sh
        - pip install gitflow
        
    - title: "Install extra php libraries"
      commands:
        # PHP
        - curl -fsSL https://phar.phpunit.de/phpunit.phar -o ${BUILD_PATHS_BINARIES_FOLDER}/phpunit.phar
        #- curl -fsSL http://static.phpmd.org/php/latest/phpmd.phar -o ${BUILD_PATHS_BINARIES_FOLDER}/phpmd.phar
        - curl -fsSL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar -o ${BUILD_PATHS_BINARIES_FOLDER}/phpcs.phar
        - curl -fsSL https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar -o ${BUILD_PATHS_BINARIES_FOLDER}/phpcbf.phar
        - curl -fsSL http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar -o ${BUILD_PATHS_BINARIES_FOLDER}/php-cs-fixer.phar
        - curl -fsSL https://getcomposer.org/composer.phar -o ${BUILD_PATHS_BINARIES_FOLDER}/composer.phar
        - curl -fsSl https://raw.githubusercontent.com/iArren/composer-bash-completion/master/composer -o ${BUILD_PATHS_BASH_COMPLETITIONS}/composer
        
    - title: "Install extra Wordpress libraries"
      commands:
        - curl -fsSL https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -o ${BUILD_PATHS_BINARIES_FOLDER}/wp-cli.phar
        - curl -fsSl https://raw.githubusercontent.com/wp-cli/wp-cli/master/utils/wp-completion.bash -o ${BUILD_PATHS_BASH_COMPLETITIONS}/wp-cli
        
    - title: "Install extra Magento libraries"
      commands:
        - curl -fsSL https://files.magerun.net/n98-magerun-latest.phar -o ${BUILD_PATHS_BINARIES_FOLDER}/n98-magerun.phar
        - curl -fsSl https://raw.githubusercontent.com/netz98/n98-magerun/master/res/autocompletion/bash/n98-magerun.phar.bash -o ${BUILD_PATHS_BASH_COMPLETITIONS}/n98-magerun
        - curl -fsSL https://raw.githubusercontent.com/mhauri/generate-modman/master/generate-modman -o ${BUILD_PATHS_BINARIES_FOLDER}/generate-modman
        - curl -fsSL https://raw.githubusercontent.com/colinmollenhour/modman/master/modman -o ${BUILD_PATHS_BINARIES_FOLDER}/modman
        
        - MAGECONFIGSYNC_LATEST=$(get-latest-release https://github.com/punkstar/mageconfigsync )
        - curl -fsSL https://github.com/punkstar/mageconfigsync/releases/download/${MAGECONFIGSYNC_LATEST}/mageconfigsync-${MAGECONFIGSYNC_LATEST}.phar -o ${BUILD_PATHS_BINARIES_FOLDER}/mageconfigsync.phar
        
        - MAGESCAN_LATEST=$(get-latest-release https://github.com/steverobbins/magescan )
        - curl -fsSL https://github.com/steverobbins/magescan/releases/download/${MAGESCAN_LATEST}/magescan.phar -o ${BUILD_PATHS_BINARIES_FOLDER}/magescan.phar
        
    - title: "Install extra Docker utilities"
      commands:
        - pip install docker-compose
        - DOCKER_COMPOSE_LATEST_VERSION=$(get-latest-release https://github.com/docker/compose/ )
        - curl -fsSl https://raw.githubusercontent.com/docker/compose/${DOCKER_COMPOSE_LATEST_VERSION}/contrib/completion/bash/docker-compose -o ${BUILD_PATHS_BASH_COMPLETITIONS}/docker-compose
        
        - pip3 install git+https://gitlab.com/alphasocket/dockerfile-builder.git
        
        - DRY_LATEST=$(get-latest-release https://github.com/moncho/dry/ )
        - curl -fsSL https://github.com/moncho/dry/releases/download/${DRY_LATEST}/dry-linux-amd64 -o ${BUILD_PATHS_BINARIES_FOLDER}/dry

        - npm install -g snyk

        # Install haskell
        - curl -fsSL https://get.haskellstack.org/ | sh
        - HADOLINT_LATEST=$(get-latest-release https://github.com/hadolint/hadolint)
        - curl -fsSL https://github.com/hadolint/hadolint/releases/download/${HADOLINT_LATEST}/hadolint-Linux-x86_64 -o ${BUILD_PATHS_BINARIES_FOLDER}/hadolint
        
    - title: "Install Cloud CLIs"
      commands:
        - pip install awscli
        - pip install gcloud
        - DOCTL_LATEST=$(get-latest-release https://github.com/digitalocean/doctl | sed 's/v//g' )
        - curl -fsSL https://github.com/digitalocean/doctl/releases/download/v$DOCTL_LATEST/doctl-$DOCTL_LATEST-linux-amd64.tar.gz -o /tmp//doctl.tar.gz
        - tar xf /tmp/doctl.tar.gz -C /usr/local/bin/

        
    - title: "Install Kubernetes utilities"
      commands:
        - KUBECTL_LATEST=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)
        - curl -fsSL https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_LATEST}/bin/linux/amd64/kubectl -o ${BUILD_PATHS_BINARIES_FOLDER}/kubectl
        - kubectl completion bash > ${BUILD_PATHS_BASH_COMPLETITIONS}/kubectl
        
        ## TODO: install kubedm
        
        - HELM_LATEST=$(get-latest-release https://github.com/kubernetes/helm/ | sed -e "s/ //g" | sed -e "s/latest//g" )
        - mkdir /tmp/helm
        - curl -fsSL "https://storage.googleapis.com/kubernetes-helm/helm-${HELM_LATEST}-linux-amd64.tar.gz" -o /tmp/helm/helm.tar.gz
        - tar -xvf /tmp/helm/helm.tar.gz -C /tmp/helm/
        - mv /tmp/helm/linux-amd64/helm ${BUILD_PATHS_BINARIES_FOLDER}/helm
        - helm completion bash > ${BUILD_PATHS_BASH_COMPLETITIONS}/helm
        
        - KOPS_LATEST=$(get-latest-release https://github.com/kubernetes/kops )
        - curl -fsSL https://github.com/kubernetes/kops/releases/download/${KOPS_LATEST}/kops-linux-amd64 -o ${BUILD_PATHS_BINARIES_FOLDER}/kops
        - kops completion bash > ${BUILD_PATHS_BASH_COMPLETITIONS}/kops
        
        - ARGO_LATEST=$(get-latest-release https://github.com/argoproj/argo )
        - curl -fsSL https://github.com/argoproj/argo/releases/download/${ARGO_LATEST}/argo-linux-amd64 -o ${BUILD_PATHS_BINARIES_FOLDER}/argo
        
        - KOMPOSE_LATEST=$(get-latest-release https://github.com/kubernetes/kompose )
        - curl -fsSL https://github.com/kubernetes/kompose/releases/download/${KOMPOSE_LATEST}/kompose-linux-amd64 -o ${BUILD_PATHS_BINARIES_FOLDER}/kompose
        - kompose completion bash > ${BUILD_PATHS_BASH_COMPLETITIONS}/kompose
        
        - curl -fsSL https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64 -o ${BUILD_PATHS_BINARIES_FOLDER}/skaffold
        
        - K3S_LATEST=$(get-latest-release https://github.com/zeerorg/k3s-in-docker)
        - curl -fsSL https://github.com/zeerorg/k3s-in-docker/releases/download/${K3S_LATEST}/k3d-arm64 -o ${BUILD_PATHS_BINARIES_FOLDER}/k3d

        - LINKERD_LATEST=$(curl -fSsL https://github.com/linkerd/linkerd2/releases 2> /dev/null| grep -oe "stable-[0-9]*\.[0-9]*\.[0-9]*" 2> /dev/null | head -n 1)
        - curl -fsSL https://github.com/linkerd/linkerd2/releases/download/${LINKERD_LATEST}/linkerd2-cli-${LINKERD_LATEST}-linux -o ${BUILD_PATHS_BINARIES_FOLDER}/linkerd

        - pip3 install -r https://raw.githubusercontent.com/kubernetes-sigs/kubespray/master/requirements.txt   
    
    - title: "Install provisioning utilities"
      commands:
        - pip install ansible pykeepass ansible-container[docker,k8s]
        - mkdir -p ~/.ansible/plugins/lookup
        - curl -fsSL https://raw.githubusercontent.com/viczem/ansible-keepass/master/keepass.py -o ~/.ansible/plugins/lookup/keepass.py
        # TODO

    - title: "Install security utilities"
      commands:
        - VAULT_LATEST=$(curl -fsSL  https://github.com/hashicorp/vault/releases/ | grep "/hashicorp/vault/releases/tag/v" | grep -Eo "[0-9]*\.[0-9]*\.[0-9]*" 2> /dev/null | head -n 1 )
        - curl -fsSL https://releases.hashicorp.com/vault/${VAULT_LATEST}/vault_${VAULT_LATEST}_linux_amd64.zip -o /tmp/vault.zip
        - unzip -d ${BUILD_PATHS_BINARIES_FOLDER} /tmp/vault.zip

        - CONSUL_LATEST=$(curl -fsSL  https://github.com/hashicorp/consul/releases/ | grep "/hashicorp/consul/releases/tag/v" | grep -Eo "[0-9]*\.[0-9]*\.[0-9]*" 2> /dev/null | head -n 1 )
        - curl -fsSL https://releases.hashicorp.com/consul/${CONSUL_LATEST}/consul_${CONSUL_LATEST}_linux_amd64.zip -o /tmp/consul.zip
        - unzip -d ${BUILD_PATHS_BINARIES_FOLDER} /tmp/consul.zip

        # OnePassword
        - mkdir /tmp/op
        - curl -fsSL https://cache.agilebits.com/dist/1P/op/pkg/v0.4/op_linux_amd64_v0.4.zip -o /tmp/op/op.zip
        - unzip /tmp/op/op.zip -d /tmp/op
        - mv /tmp/op/op ${BUILD_PATHS_BINARIES_FOLDER}/op
        - rm -r /tmp/op
        
    - title: "Install testing utilities"
      commands:
        - mkdir /tmp/browserstack
        - curl -fsSL https://www.browserstack.com/browserstack-local/BrowserStackLocal-linux-x64.zip -o /tmp/browserstack/BrowserStackLocal.zip
        - unzip /tmp/browserstack/BrowserStackLocal.zip -d /tmp/browserstack
        - mv /tmp/browserstack/BrowserStackLocal ${BUILD_PATHS_BINARIES_FOLDER}/BrowserStackLocal
        - rm -r /tmp/browserstack
        
        - npm install -g cypress --save-dev

    - title: "Install SSL utilities"
      commands:
        - go get -u github.com/cloudflare/cfssl/cmd/cfssl
        - go get -u github.com/cloudflare/cfssl/cmd/cfssljson
        
    - title: "Install custom Snippets"
      commands:
        - git -C ${BUILD_PATHS_LIBRARIES_FOLDER}/ clone https://gitlab.com/alphasocket/snippets
        - for bin in $(find ${BUILD_PATHS_LIBRARIES_FOLDER}/snippets -mindepth 1 -maxdepth 1 -type f ); do
            mv ${bin} ${BUILD_PATHS_BINARIES_FOLDER}/;
          done
        - git -C ${BUILD_PATHS_LIBRARIES_FOLDER}/snippets checkout --force

  processes_after:
    - title: "Final config"
      commands:
        - chmod -R +x ${BUILD_PATHS_BINARIES_FOLDER}
        -  export PAGER=less
        
entrypoint:
  processes:
    - title: "Welcome to alphasocket/toolbox"
      commands:
        - '[ "$ENTRYPOINT_VERBOSE" = "$GENERAL_KEYS_TRUE" ] && [ -f /var/tmp/.welcome ] && cat /var/tmp/.welcome'
        
#
# Test
#
test:
  processes:
    - title: "Container binaries"
      commands:
        - git --version
        - git flow version
        - hg version
        #- hg flow version
        - php -v
        - n98-magerun.phar --version
        - composer.phar --version
        - wp-cli.phar --version
        - kubectl version --client
        - helm version --client
        - op --version
        - k3d --version
        - hadolint --version
        - vault --version
        - consul --version
        - cfssl --version
        - cfssljson --version
